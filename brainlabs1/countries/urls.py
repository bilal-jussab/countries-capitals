from . import views
from django.conf.urls import url

urlpatterns = [
    url(
        r'^$',
        views.CountriesIndexView.as_view(),
        name='index'
    ),
    url(
        r'^question/',
        views.CountriesQuestionPageView.as_view(),
        name='question'
    ),
]
