from django import forms


class QuestionForm(forms.Form):
    country = forms.CharField(max_length=100)
    capital = forms.CharField(max_length=100)

    def __init__(self, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['country'].widget.attrs['readonly'] = True
