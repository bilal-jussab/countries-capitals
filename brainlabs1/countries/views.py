import random

from django.shortcuts import render
from .forms import QuestionForm

import requests
from django.views.generic import FormView, TemplateView


API_ENDPOINT = 'https://countriesnow.space/api/v0.1/countries/capital'


class CountriesIndexView(TemplateView):
    template_name = 'countries/index.html'


class CountriesQuestionPageView(FormView):
    template_name = 'countries/question.html'
    form_class = QuestionForm

    def get_initial(self):
        initial = super().get_initial()
        countries = get_country_list()
        initial['country'] = random.choice(countries)
        return initial

    def form_valid(self, form):
        country = form.cleaned_data['country']
        capital = form.cleaned_data['capital']

        is_actually_valid, answer = check_answer(country, capital)
        my_kwargs = {'answer': answer}
        if not is_actually_valid:
            return self.form_invalid(self.request, **my_kwargs)
        return render(
            self.request,
            'countries/submission.html',
            {'message': '[✓] You answered correctly'}
        )

    def form_invalid(self, form, **kwargs):
        answer = kwargs.get('answer')
        return render(
            self.request,
            'countries/submission.html',
            {'message': '[X] You answered incorrectly. The correct answer is {}'.format(answer)}
        )


def get_country_list():
    response = requests.get(API_ENDPOINT)
    data = response.json()
    return [country['name'] for country in data['data']]


def get_capital_mapping():
    response = requests.get(API_ENDPOINT)
    data = response.json()
    capital_mapping = {}
    for country in data['data']:
        capital_mapping[country['name']] = country['capital']
    return capital_mapping


def check_answer(country, capital):
    mapping = get_capital_mapping()
    correct_capital = mapping.get(country)
    if capital.lower() == correct_capital.lower():
        return True, capital
    return False, correct_capital
