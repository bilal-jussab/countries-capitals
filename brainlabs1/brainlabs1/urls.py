"""brainlabs1 URL Configuration
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('countries/', include('countries.urls')),
    path('admin/', admin.site.urls),
]